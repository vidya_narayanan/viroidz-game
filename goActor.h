#include "gameObject.h"
#include "goBullet.h"
#define BULLETCHANGE_FREQUENCY 1 //If the bullet type changes, for how long should the effect last
//The main Shooter class derived from Body

class Actor:public Body
{
	
	float mActorSpeed; //Speed of Forward Movement
	BulletType mBulletType; //Type of Bullet fired, can change based on powerup
	bool mIsShielded;//Is the actor shielded?
	double mShieldSince; //Since when is the actor shielded
	double mShieldTill; //Until when should the actor be shielded
	double mLastFiredTime; //Last Bullet fired time, to calculate recoil time
	double mLastBulletChangedTime;//Last Bullet Changed time, to calculate time for reverting back to default
public:
	Actor(Point position, float angle,char* texture,int strength); //Constructor
	void Update();
	void Render();
	~Actor();
	
	void SpecialKeyboard(int theKey, int mouseX, int mouseY); //Handles keyboard input for the actor
	void SetShield(double from,double to);
};
