#include "gameObject.h"
typedef enum bulletType
{
	BULLET_DEFAULT=0, //Default Bullet Fires straight along Actors forward vector
	BULLET_MISSILE,   //TBD:Picks up the first available target's position 
					  //in the gameobject list and follows a path to it 
	BULLET_STICKY,    //TBD: Sticks to the Target for a specified time, explodes
				      //affects a radius around it
	BULLET_BOUNCY,	  //TBD: Is not destroyed on reaching boundary, bounces back once	
	BULLET_RAPID,	  //TBD: Greater Speed
}BulletType;
class Bullet:public Body
{
	BulletType mType;
	
	public:
	bool mIsEnemy; //Is being fired by a target, not Actor
	Bullet(Point position, float angle,char * texture,BulletType type,bool enemy=false);
	void Update();
	void Render();
	~Bullet();
};