#include "goPowerUp.h"

#include <time.h>
PowerUp::PowerUp(Point position, float angle,char* texture,PowerUpType type,double spawntime,double staytime)
{
	mBodyType = Obj_PowerUp;
	mPosition = position;
	mAngle = angle;
	//load texture
	mPowerUpType = type;
	mSpawnTime = spawntime;
	mStayTill = staytime;
	if(!LoadTGA(&mTex, texture))
	{
		printf("\nPowerup Texture Loading Problem");
		return;
	}
	printf("\nPower up Loaded Texture successfully");
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &mTex.texID);
	glBindTexture(GL_TEXTURE_2D,mTex.texID);
	glTexImage2D(GL_TEXTURE_2D, 0, mTex.bpp / 8, mTex.width, mTex.height, 0, mTex.type, GL_UNSIGNED_BYTE, mTex.imageData);
	if (mTex.imageData) 
	{
		free(mTex.imageData);
	}

}
void PowerUp::SetErase()
{
	mStayTill = 0;
}
bool PowerUp::ShouldErase()
{
	if((time(NULL)-mSpawnTime) > mStayTill)
		return true;
	return false;
}
void PowerUp::Render(){
glPushMatrix();
glLoadIdentity();
gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);
	
DrawRectangle(mTex,mPosition.x,mPosition.y,20,20,mAngle);


glPopMatrix();

}
void PowerUp::Update(){
//Can Delete it here, but will have to update the g_GameObjects again, 
//So will do it now in the main loop itself, TBD:make this cleaner

}
PowerUp::~PowerUp(){
//free texture
}
