#include "goActor.h"
#include "goTarget.h"
#include "goPowerUp.h"
#include <time.h>
#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>	
#define FIRE_FREQUENCY 500 // Tick difference between consecutive shoots
Actor::Actor(Point position, float angle, char* texture,int strength)
{
	mBodyType = Obj_Actor; 
	mPosition = position;
	mAngle = 0;
	
	mLastFiredTime=0;
	mStrength = strength; //Life
	 
	 mActorSpeed = 2.0f; //Speed
	 mBulletType = BULLET_DEFAULT; 
	if(!LoadTGA(&mTex, texture))
	{
		printf("\nActor Texture Loading Problem");
		return;
	}
	printf("\nLoaded Texture successfully");
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &mTex.texID);
	
	glBindTexture(GL_TEXTURE_2D,mTex.texID);
	glTexImage2D(GL_TEXTURE_2D, 0, mTex.bpp / 8, mTex.width, mTex.height, 0, mTex.type, GL_UNSIGNED_BYTE, mTex.imageData);
	if (mTex.imageData) 
	{
		free(mTex.imageData);
	}	
	mIsShielded = false;

}
void Actor::SpecialKeyboard(int theKey, int mouseX, int mouseY)
{
	if(theKey==VK_RIGHT)
	{
		mAngle-=(1);
		
	}
	else if(theKey==VK_LEFT)
	{
		mAngle+=(1);
		
	}
	if(theKey==VK_UP)
	{
		//move along forward vector
		float xVector = mPosition.x; 
		float yVector = mPosition.y;
		float xVector2 = mActorSpeed*cos(TO_RADIAN(mAngle+90))+xVector;
		float yVector2 = mActorSpeed*sin(TO_RADIAN(mAngle+90))+yVector;
		mPosition.x = xVector2; mPosition.y =yVector2;

	}
	/*else if(theKey==VK_DOWN)
	{
			float xVector = mPosition.x; 
		float yVector = mPosition.y;
		float xVector2 = -2*cos(TO_RADIAN(mAngle+90))+xVector;
		float yVector2 = -2*sin(TO_RADIAN(mAngle+90))+yVector;
			mPosition.x = xVector2; mPosition.y =yVector2;
		
	}*/
	if(theKey== VK_SPACE && (GetTickCount()-mLastFiredTime)>FIRE_FREQUENCY)
		{
			
			mLastFiredTime=GetTickCount();
			Point bulletPt;
			 bulletPt.x = 15*cos(TO_RADIAN(mAngle+90))+mPosition.x; 
			 bulletPt.y = 15*sin(TO_RADIAN(mAngle+90))+mPosition.y;
			Bullet *bullet = new Bullet(bulletPt,mAngle,".\\DATA\\Bullet.tga",mBulletType);
			g_GameObjectsList.push_back(bullet);
		}

}


void Actor::Render(){
glPushMatrix();
glLoadIdentity();
gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);
	
DrawRectangle(mTex,mPosition.x,mPosition.y,40,40,mAngle);
glPopMatrix();
glPushMatrix();
glLoadIdentity();
gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);
/*//Debug Forward Vector
glLineWidth(3);
float xVector = mPosition.x; 
float yVector = mPosition.y;
float xVector2 = 10*cos(TO_RADIAN(mAngle+90))+xVector;
float yVector2 = 10*sin(TO_RADIAN(mAngle+90))+yVector;
xVector = (((xVector-0)/(SCRWIDTH-0))*2)-1;
yVector = (((yVector-0)/(SCRHEIGHT-0))*2)-1;
xVector2 = (((xVector2-0)/(SCRWIDTH-0))*2)-1;
yVector2 = (((yVector2-0)/(SCRHEIGHT-0))*2)-1;
glDisable(GL_TEXTURE_2D);
glDisable(GL_BLEND);
glColor4f(1,0,0,1);
glBegin(GL_LINES);
glVertex3f(xVector,yVector,0);
glVertex3f(xVector2,yVector2,0);
glEnd();*/

//Render A shield (Yellow Circle, around actor)
if(mIsShielded)
{
glPushMatrix();
glLoadIdentity();
gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);
glLineWidth(2);
float xVector = mPosition.x; 
float yVector = mPosition.y;

xVector = (((xVector-0)/(SCRWIDTH-0))*2)-1;
yVector = (((yVector-0)/(SCRHEIGHT-0))*2)-1;
glColor3f(1,1,0);
	float radius = 0.1f;
	glTranslatef(xVector,yVector,0);
	glBegin(GL_LINE_LOOP);
	for(int i = 0; i<360;i+=10)
	{
		glVertex3f(cos(TO_RADIAN(i))*radius,sin(TO_RADIAN(i))*radius,0);
		
	}
	glEnd();
glDisable(GL_TEXTURE_2D);
glDisable(GL_BLEND);

}


glPopMatrix();
}
void Actor::SetShield(double from,double to)
{
	mIsShielded = true;
	mShieldSince = from;
	mShieldTill = to;
	printf("\nShield Set");
}
void Actor::Update(){
	
	if(mStrength>3)mStrength=3; //Max strength = 3
	if(mIsShielded)
	{
		if(mShieldTill<time(NULL))
		{
			mIsShielded=false;
			printf("\nShield Removed"); //Remove Shield After shield time is over
		}
	}
	for(unsigned int i=0; i<g_GameObjectsList.size();i++)
	{
		//Check for collision with target
		if(g_GameObjectsList[i]->mBodyType==Obj_Target)
		{
			Target *tar = (Target*)g_GameObjectsList[i];
			if(RectContainsPoint(tar->mPosition,tar->mStrength*tar->mRadius,tar->mStrength*tar->mRadius,mPosition)&&!mIsShielded)
			{
				mStrength--;
				SetShield(time(NULL),time(NULL)+2);
				printf("\nActor Strength: %d",mStrength);
			}
		}
		else if(g_GameObjectsList[i]->mBodyType==Obj_Bullet )
		{
			Bullet *bullet = (Bullet*)g_GameObjectsList[i];
			//Check for collision with targets bullet
			if(!bullet->mIsEnemy)
				return;
			
			if(RectContainsPoint(mPosition,40,40,bullet->mPosition)&&!mIsShielded)
			{
				mStrength--;
				SetShield(time(NULL),time(NULL)+2);
				printf("\nActor Strength: %d",mStrength);
			}
		}
		else if(g_GameObjectsList[i]->mBodyType==Obj_PowerUp)
		{
			//Check for collision with powerup
			if(RectContainsPoint(g_GameObjectsList[i]->mPosition,20,20,mPosition))
			{
				PowerUp *p =(PowerUp*) g_GameObjectsList[i];
				switch(p->mPowerUpType)
				{
				case POWERUP_LIFE:
					{
						mStrength++;
						p->SetErase();
					}
					break;
				default:
					break;
				}
			}
		}
	}
}
Actor::~Actor(){
//free texture
	
}
