This code was written long long ago over a weekend. A very basic replica of http://www.mousebreaker.com/games/viroidz
in C++, OpenGL, Gimp. (Windows only)
========================================================================
    GAME: VIROIDZ 
========================================================================
Overview:

Levels are generated randomly. To jump to a level press the level number button(1-9)
FEATURES REPLICATED FROM ORIGINAL GAME:
1.Character Movement & Firing & Reflection
2.Target Objects that
	1.Divide on being hit
	2.Replicate after time
	3.Shoot bullets that affect the actor
3.Basic Bullets (Moves along forward Vector)
4.Basic Power Up (Life)
5.UI:
	1.Shows the Lives Remaining of the Actor
6.ART: Created some place holder art	
	
FEATURES TO BE ADDED:
IN GAME:
1. Additional Bullet Types
2. Additional Power up types and timing
3. Non Random Levels
4. Additional Settings
5. Sounds
6. Sprite Animation
7. Scoring

KNOWN ISSUES:
1. Since levels are generated randomly, on failing a level the same level is not regenerated
2. Update frequency/elapsed time is not calculated by processcycle time
   Use of tickcounts as a timer in certain places may giver processor dependent game speed
3. Does not handle window resize
4. Textures have to be power of 2 
   
Declaration:
All Code(Unless mentioned otherwise eg:Texture Loader) has been written by me.
All Artwork has been done by me. Free fonts have been used.