#ifndef __GO
#define __GO
#include "basics.h"
#include "ResourceManager.h"
#include <string>
#include <vector>

using namespace std;

//GameObject Type maintained to query and identify 
typedef enum bodyType{
	Obj_Actor=0,//Main Game Object/Shooter
	Obj_Target,//Bodies that have to be killed 
	Obj_Bullet,
	Obj_BG,//Background
	Obj_PowerUp,//Power Ups
	Obj_Invalid
}BodyType;

//BaseClass Body from which all GameObjects are derived
//Holds its position, angle ,strength and texture
//Strength for an Actor(Shooter) indicates lives
//Strength for a Target is utilized as health,size factor and speed factor
//i.e a target of strength 2 has to be shot twice to be killed, at strength 1 its 
//size is smaller and speed is greater
class Body
{
public:
BodyType mBodyType;
Point mPosition;
float mAngle;
int mStrength; 
Texture mTex;
bool isOutOfBound();
virtual void Render();
virtual void Update();
virtual ~Body();

virtual void SpecialKeyboard(int theKey, int mouseX, int mouseY);
};

extern std::vector<Body*> g_GameObjectsList; //Main Object List that holds all game objects

#endif