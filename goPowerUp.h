#include "gameObject.h"
typedef enum powerUp{
	POWERUP_LIFE=0,//Increases Actors Strength by 1
	POWERUP_SHIELD,//TBD:Sets Actors Shield for a duration
	POWERUP_GLUEBOMB,//TBD:Changes Actors Bullet type to BULLET_STICKY
	POWERUP_MISSILE,//TBD:Changes Actors Bullet type to MISSILE and so on
}PowerUpType;
class PowerUp:public Body
{
	
	double mSpawnTime;//When was the power up spawned
	double mStayTill;//Until when should the power up last
	public:
	PowerUpType mPowerUpType;
	PowerUp(Point position, float angle,char* texture,PowerUpType type,double spawntime,double staytime);
	 void Render();
	 void Update();
	 bool ShouldErase();
	 void SetErase();
	~PowerUp();
};
