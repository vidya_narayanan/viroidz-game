#ifndef __BASICS
#define __BASICS
#include <windows.h>	
#include <math.h>	
//Point class to hold Position values
struct Point
{
	float x;
	float y;
};
//Default Screenwidth & Height
#define SCRWIDTH 800.0
#define SCRHEIGHT 800.0
//Math Helper Macros
#define MATH_PI (22.0/7.0)
#define TO_RADIAN(x) ((x)*0.0174532925)
#define TO_DEGREE(x) ((x)*57.2957795)

#endif