#include "gameObject.h"
//This class could have been avoided for the given case
//Could be useful if backrounds are scrolling and different according to levels etc.,
class BG:public Body
{
	public:
	BG(char* texture);
	void Update();
	void Render();
	~BG();
};