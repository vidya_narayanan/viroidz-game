#include "goTarget.h"
#include "goBullet.h"
#include <time.h>
Target::Target(Point position, float angle,char* texture,int strength,double spawnTime,double divideInSeconds,bool canDivide,bool canShoot)
{
	mBodyType = Obj_Target;
	mPosition = position;
	mAngle = angle;
	mCanDivide = canDivide;
	mLastDividedTime = spawnTime;
	mDivideIn = divideInSeconds;
	mCanShoot = canShoot;
	//load texture
	mStrength = strength;
	if(!LoadTGA(&mTex, texture))
	{
		printf("\nActor Texture Loading Problem");
		return;
	}
	printf("\nLoaded Texture successfully");
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &mTex.texID);
	glBindTexture(GL_TEXTURE_2D,mTex.texID);
	
	glTexImage2D(GL_TEXTURE_2D, 0, mTex.bpp / 8, mTex.width, mTex.height, 0, mTex.type, GL_UNSIGNED_BYTE, mTex.imageData);
	if (mTex.imageData) 
	{
		free(mTex.imageData);
	}
	mRadius = 40;

}

void Target::Render(){
glPushMatrix();
glLoadIdentity();
gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);
	
DrawRectangle(mTex,mPosition.x,mPosition.y,mStrength*mRadius,mStrength*mRadius,mAngle);


glPopMatrix();

}
void Target::Replicate(int strength)
{
	char *target;
	if(mCanDivide)
		target = ".\\DATA\\target1.tga";
	else if(mCanShoot)
		target =".\\DATA\\target3.tga";
	else 
		target=".\\DATA\\target2.tga";
	Target *newtarget = new Target(mPosition,rand()%360,target,(strength==-1)?mStrength:strength,time(NULL),mDivideIn,mCanDivide,mCanShoot);
	g_GameObjectsList.push_back(newtarget);
	mAngle = rand()%360; //randomize current angle
}
void Target::Update(){
	if(mStrength==0)
		return;

	if(mCanDivide&&((time(NULL) - mLastDividedTime)> mDivideIn))
	{
		Target *newTarget = new Target(mPosition,rand()%360,".\\DATA\\target1.tga",mStrength,time(NULL),mDivideIn,true);
		g_GameObjectsList.push_back(newTarget);
		mLastDividedTime = time(NULL);
	}
float xVector = mPosition.x;
float yVector = mPosition.y;
float xVector2 = (1.0/mStrength)*cos(TO_RADIAN(mAngle+90))+xVector;	
float yVector2 = (1.0/mStrength)*sin(TO_RADIAN(mAngle+90))+yVector;
mPosition.x = xVector2; mPosition.y =yVector2;

if((time(NULL)-mLastShootTime)>SHOOT_FREQUENCY && mCanShoot)
{
	mLastShootTime = time(NULL);
	Point bulletPt;
	bulletPt.x = 15*cos(TO_RADIAN(mAngle+90))+mPosition.x; 
	bulletPt.y = 15*sin(TO_RADIAN(mAngle+90))+mPosition.y;
	Bullet *bullet = new Bullet(bulletPt,mAngle,".\\DATA\\Arrow.tga",BULLET_DEFAULT,true);
	g_GameObjectsList.push_back(bullet);
}
}
Target::~Target(){
//free texture
}
