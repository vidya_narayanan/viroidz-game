#include "goBG.h"
BG::BG(char* texture)
{
	mBodyType = Obj_BG;
	mPosition.x= SCRWIDTH/2;mPosition.y= SCRHEIGHT/2;
	if(!LoadTGA(&mTex, texture))
	{
		printf("\nBackground Texture Loading Problem");
		return;
	}
	printf("\nBackground Loaded Texture successfully");
	glGenTextures(1, &mTex.texID);
	glBindTexture(GL_TEXTURE_2D,mTex.texID);
	
	glTexImage2D(GL_TEXTURE_2D, 0, mTex.bpp / 8, mTex.width, mTex.height, 0, mTex.type, GL_UNSIGNED_BYTE, mTex.imageData);

	if (mTex.imageData) 
	{
		free(mTex.imageData);
	}	
	
}
//Render a fullscreen texture
void BG::Render(){
	glPushMatrix();
	glLoadIdentity();
	gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);

	DrawRectangle(mTex,SCRWIDTH/2,SCRHEIGHT/2,SCRWIDTH,SCRHEIGHT,0);
glPopMatrix();
}
void BG::Update(){}
BG::~BG(){
//free texture
	
}