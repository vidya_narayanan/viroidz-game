
#include "basics.h"
#include "Texture.h"
#include <string.h>
//Helper function for Collision
static bool RectContainsPoint(Point rectCenter,float w,float h, Point pt)
{
	if(((rectCenter.x - w/2 )< pt.x) && ((rectCenter.x + w/2) > pt.x) && ((rectCenter.y - h/2)<pt.y) && ((rectCenter.y+h/2)>pt.y))
		return true;
return false;
}
//Helper function for drawing an Alpha Blended Texture
static void DrawRectangle(Texture tex,float x ,float y,float width,float height,float angle)
{
//Mapping Values from 0->SCRWIDTH 0->SCRHEIGHT to -1->1 -1->1

width = 2*width/SCRWIDTH;
height = 2*height/SCRHEIGHT;


x = (((x-0)/(SCRWIDTH-0))*2)-1;
y = (((y-0)/(SCRHEIGHT-0))*2)-1;

glPushMatrix();
glLoadIdentity();

gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);

//glRotatef(angle,0,0,1);
glTranslatef(x,y,0);

glRotatef(angle,0,0,1);

glEnable(GL_TEXTURE_2D);
glBindTexture(GL_TEXTURE_2D, tex.texID);

glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
glEnable(GL_BLEND);
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



glBegin(GL_QUADS);
/*glTexCoord2f(0.0, 0.0); glVertex2f(-0.1, 0.0);
glTexCoord2f(1.0, 0.0); glVertex2f(0.1, 0.0);
glTexCoord2f(1.0, 1.0); glVertex2f(0.1, 0.1);
glTexCoord2f(0.0, 1.0); glVertex2f(-0.1,0.1);
*/
glTexCoord2f(0.0, 0.0); glVertex2f(-width/2,- height/2);
glTexCoord2f(1.0, 0.0); glVertex2f(width/2, - height/2);
glTexCoord2f(1.0, 1.0); glVertex2f(width/2, height/2);
glTexCoord2f(0.0, 1.0); glVertex2f(-width/2,height/2);


glEnd();
glDisable(GL_TEXTURE_2D);
glDisable(GL_BLEND);
glBindTexture(GL_TEXTURE_2D,0);
glPopMatrix();
}
