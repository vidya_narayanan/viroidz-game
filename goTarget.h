#include "gameObject.h"
#define SHOOT_FREQUENCY 1 //If the Target can shoot, the frequeny of the same
class Target:public Body
{
	bool mCanDivide; //Can the target Replicate
	double mDivideIn;//After how much time should the target Replicate
	double mLastDividedTime;//When did it last replicate
	bool mCanShoot;//Can the target Shoot
	double mLastShootTime;//When was the last bullet shot
	public:
		float mRadius; //Radius of the target, actual width height = radius*strength
		
	Target(Point position, float angle,char* texture,int strength,double spawnTime=0,double divideInSeconds=0,bool canDivide=false,bool canShoot=false);
	 void Render();
	 void Update();
	 void Replicate(int strength=-1);
	~Target();
};
