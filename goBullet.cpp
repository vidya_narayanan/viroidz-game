#include "goBullet.h"
#include "goTarget.h"
#include "goPowerUp.h"
#include <time.h>
Bullet::Bullet(Point position, float angle, char* texture,BulletType type,bool enemy)
{
	mBodyType = Obj_Bullet;
	mPosition = position;
	mAngle = angle;
	//load texture
	mType = type;
	mIsEnemy = enemy;
	if(!LoadTGA(&mTex, texture))
	{
		printf("\nBullet Texture Loading Problem");
		return;
	}
	printf("\nBullet Loaded Texture successfully");
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &mTex.texID);
	glBindTexture(GL_TEXTURE_2D,mTex.texID);
	glTexImage2D(GL_TEXTURE_2D, 0, mTex.bpp / 8, mTex.width, mTex.height, 0, mTex.type, GL_UNSIGNED_BYTE, mTex.imageData);
	if (mTex.imageData) 
	{
		free(mTex.imageData);
	}
	
}
void Bullet::Render(){
glPushMatrix();
	glLoadIdentity();
	gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);

switch(mType)
{
case BULLET_DEFAULT:
	{
		DrawRectangle(mTex,mPosition.x,mPosition.y,10,10,mAngle);
	}
	break;
case BULLET_MISSILE:
	{
		//TBD
	}
	break;
default:
	{
		printf("Bullet type incorrect");
	}
	break;
}
glPopMatrix();
}
void Bullet::Update(){
	switch(mType)
	{
	case BULLET_DEFAULT:{
		float xVector = mPosition.x; 
		float yVector = mPosition.y;
		float xVector2 = 2*cos(TO_RADIAN(mAngle+90))+xVector;
		float yVector2 = 2*sin(TO_RADIAN(mAngle+90))+yVector;
		mPosition.x = xVector2; mPosition.y =yVector2;}
						break;
	case BULLET_MISSILE:{//TBD
		Target *missileTarget;
		for(unsigned int i=0;i<g_GameObjectsList.size();i++)
		{
			if(g_GameObjectsList[i]->mBodyType== Obj_Target)
			{
				missileTarget = (Target *)g_GameObjectsList[i];
			}
		}
		}break;
	}
for(unsigned int i=0;i <g_GameObjectsList.size();i++)
{
	if(g_GameObjectsList[i]->mBodyType==Obj_Target && !mIsEnemy)
	{
		//Check for collision with Target
		Target *target =(Target *) g_GameObjectsList[i];
		if(RectContainsPoint(target->mPosition,target->mStrength*target->mRadius,target->mStrength*target->mRadius,mPosition))
		{
			printf("/nBullet Found a body");
			
			target->mStrength--;
			if(target->mStrength > 0)
			{
				target->Replicate();
				
			}
			int k=0;
			if(target->mStrength <= 0)
			{
				//Create a powerup here, Currently life powerups are created whenever a target is deleted
				PowerUp *p = new PowerUp(mPosition,0,".\\DATA\\powerLife.tga",POWERUP_LIFE,time(NULL),5);
				g_GameObjectsList.push_back(p);
				
				delete target;
				g_GameObjectsList.erase(g_GameObjectsList.begin()+i);
			
			}
			
			for(unsigned k=0; k<g_GameObjectsList.size();k++)
			{
				if(g_GameObjectsList[k]==this)
				{
					delete this;
					g_GameObjectsList.erase(g_GameObjectsList.begin()+k);
					break;
				}
			}
		}
	}
}
}
Bullet::~Bullet(){
//free texture
	
}
