// Game.cpp : Entry Point
// Base Code for OpenGL Set up picked up from Nehe Tut1
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#include "stdafx.h"
#include <stdlib.h>
#include <math.h>
#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "basics.h"
#include "gameObject.h"
#include "goActor.h"
#include "goTarget.h"
#include "goBG.h"
#include "goPowerUp.h"
#include <vector>
using namespace std;
#include <time.h>
/***************Game Globals*******************/
std::vector<Body*> g_GameObjectsList;  //Main List that holds all the gameObjects
Actor *g_Actor;						   //Pointer to Main Game Shooter/Actor
bool g_LevelFailed;					   //Did user fail level
bool g_LevelCompleted;				   //Did user complete level 
int g_levelNumber=1;

Texture gTexLevelCompleted; //Maintaining Texture for level completion message
Texture gTexLevelFailed;	//Maintaining Texture for level failed message

/***********************************************/
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application

bool	keys[256];			// Array Used For The Keyboard Routine
bool	active=TRUE;		// Window Active Flag Set To TRUE By Default
bool	fullscreen=FALSE;	// Fullscreen Flag Set To Fullscreen Mode By Default
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc


/*************** Window Funcs**************/
int changeSize(int w,int h);							//Resize window
GLvoid KillGLWindow(GLvoid)								// Properly Kill The Window
{
	if (fullscreen)										// Are We In Fullscreen Mode?
	{
		ChangeDisplaySettings(NULL,0);					// If So Switch Back To The Desktop
		ShowCursor(TRUE);								// Show Mouse Pointer
	}

	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,L"Release Of DC And RC Failed.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,L"Release Rendering Context Failed.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,L"Release Device Context Failed.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,L"Could Not Release hWnd.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass(L"Game",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,L"Could Not Unregister Class.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}
int InitGL(GLvoid)										// All Setup For OpenGL Goes Here
{
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);				// Black Background
									
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	return TRUE;										// Initialization Went OK
}

BOOL CreateGLWindow(char* title, int width, int height, int bits, bool fullscreenflag)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	fullscreen=fullscreenflag;			// Set The Global Fullscreen Flag

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= L"Game";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,L"Failed To Register The Window Class.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;											// Return FALSE
	}
	
	if (fullscreen)												// Attempt Fullscreen Mode?
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth	= width;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight	= height;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel	= bits;					// Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			if (MessageBox(NULL,L"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?",L"NeHe GL",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				fullscreen=FALSE;		// Windowed Mode Selected.  Fullscreen = FALSE
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL,L"Program Will Now Close.",L"ERROR",MB_OK|MB_ICONSTOP);
				return FALSE;									// Return FALSE
			}
		}
	}

	if (fullscreen)												// Are We Still In Fullscreen Mode?
	{
		dwExStyle=WS_EX_APPWINDOW;								// Window Extended Style
		dwStyle=WS_POPUP;										// Windows Style
		ShowCursor(FALSE);										// Hide Mouse Pointer
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
		dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								L"Game",							// Class Name
								L"Viroidz",								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,L"Window Creation Error.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		bits,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,L"Can't Create A GL Device Context.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,L"Can't Find A Suitable PixelFormat.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,L"Can't Set The PixelFormat.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(hRC=wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,L"Can't Create A GL Rendering Context.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if(!wglMakeCurrent(hDC,hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,L"Can't Activate The GL Rendering Context.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	changeSize(width, height);					// Set Up Our Perspective GL Screen

	if (!InitGL())									// Initialize Our Newly Created GL Window
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,L"Initialization Failed.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	return TRUE;									// Success
}

LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_ACTIVATE:							// Watch For Window Activate Message
		{
			if (!HIWORD(wParam))					// Check Minimization State
			{
				active=TRUE;						// Program Is Active
			}
			else
			{
				active=FALSE;						// Program Is No Longer Active
			}

			return 0;								// Return To The Message Loop
		}

		case WM_SYSCOMMAND:
		{
			switch (wParam)
			{
				case SC_SCREENSAVE:
				case SC_MONITORPOWER:
					return 0;
			}
			break;
		}

		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}

		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = TRUE;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}

		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = FALSE;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}

		case WM_SIZE:								// Resize The OpenGL Window
		{
			changeSize(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

/**********************************************/
void initGameObjects()
{
	//If the level was completed, increment level number
	if(g_LevelCompleted)
		g_levelNumber++;
	g_LevelFailed = false;
	g_LevelCompleted = false;
	//Create BG Object
	BG *bg = new BG(".\\DATA\\bg1.tga");
	g_GameObjectsList.push_back(bg);
	//Actor
	Point pos; pos.x=400;pos.y=400;
	Actor *actor = new Actor(pos,0,".\\DATA\\Actor.tga",3);
	actor->SetShield(time(NULL),time(NULL)+5);
	g_GameObjectsList.push_back(actor);
	g_Actor = actor;
	//Targets
	//NOTE: Currently, levels are randomly generated based on level number
	//From level 3 onwards, Targets that multiply are introduced
	//From level 5 onwards, Targets that shoot are introduced
	
	for(int i=0;i<g_levelNumber+1;i++)
	{
	Point t1; t1.x = rand()%400;t1.y=rand()%600;
	Target *target;
	if((g_levelNumber>5 )&& (i%5==0))
		target = new Target(t1,rand()%360,".\\DATA\\target3.tga",rand()%2+1,0,0,false,true);
	else if(g_levelNumber>3 && (i%3==0))
		target = new Target(t1,rand()%360,".\\DATA\\target1.tga",2,time(NULL),10,true);
	else
		target = new Target(t1,rand()%360,".\\DATA\\target2.tga",rand()%2+1);
	
	

	if(target->isOutOfBound())
	{i--; continue;} //dirty hack to make sure targets are in circle, generate according to coordinates later
	g_GameObjectsList.push_back(target);
	}

	//Textures for level completed and level failed
	if(!LoadTGA(&gTexLevelCompleted, ".\\DATA\\LevelCompleted.tga"))
	{
		printf("\nLevelCompleted Texture Loading Problem");
		return;
	}
	printf("\nLevel Completion Texture Loaded successfully");
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &gTexLevelCompleted.texID);
	glBindTexture(GL_TEXTURE_2D,gTexLevelCompleted.texID);
	glTexImage2D(GL_TEXTURE_2D, 0, gTexLevelCompleted.bpp / 8, gTexLevelCompleted.width, gTexLevelCompleted.height, 0, gTexLevelCompleted.type, GL_UNSIGNED_BYTE, gTexLevelCompleted.imageData);
	if (gTexLevelCompleted.imageData) 
	{
		free(gTexLevelCompleted.imageData);
	}	

		//Textures for level completed and level failed
	if(!LoadTGA(&gTexLevelFailed, ".\\DATA\\LevelFailed.tga"))
	{
		printf("\nLevelFailed Texture Loading Problem");
		return;
	}
	printf("\nLevel Failed Texture Loaded successfully");
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &gTexLevelFailed.texID);
	glBindTexture(GL_TEXTURE_2D,gTexLevelFailed.texID);
	glTexImage2D(GL_TEXTURE_2D, 0, gTexLevelFailed.bpp / 8, gTexLevelFailed.width, gTexLevelFailed.height, 0, gTexLevelFailed.type, GL_UNSIGNED_BYTE, gTexLevelFailed.imageData);
	if (gTexLevelFailed.imageData) 
	{
		free(gTexLevelFailed.imageData);
	}	
}

void resetGame()
{
	//Delete all objects
	for(unsigned int i=0;i<g_GameObjectsList.size();i++)
		delete g_GameObjectsList[i];
	//Clear the list
	g_GameObjectsList.clear();
	//Restart level, level number is incremented if level was completed
	initGameObjects();
}
int changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if(h == 0)
		h = 1;

	float ratio = 1.0* w / h;

	// Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// Set the viewport to be the entire window
    glViewport(0, 0, w, h);

	glOrtho(-1,1,-1,1,0.1,100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);
return TRUE;

}



void SpecialKeyboard(int theKey, int mouseX, int mouseY) {

	if(g_LevelCompleted||g_LevelFailed)
	{resetGame();return;}
	//Pass on this data to all objects, let them handle it if they want to
    for(unsigned int i=0;i<g_GameObjectsList.size();i++)
	{
		g_GameObjectsList[i]->SpecialKeyboard(theKey,mouseX,mouseY);
	}
	
}

void drawUI()
{
	//ACTOR LIVES
	for(int i=0;i<g_Actor->mStrength;i++)
	{
		DrawRectangle(g_Actor->mTex,640+i*50,520,50,50,0);
	}
	//LEVEL FAILED MESSAGE
	if(g_LevelFailed)
	{
		DrawRectangle(gTexLevelFailed,400,400,512,128,0);
	}
	//LEVEL COMPLETED MESSAGE
	if(g_LevelCompleted)
	{
		DrawRectangle(gTexLevelCompleted,400,400,512,128,0);
	}

}
//Quick fix to change levels, press 1-9 to go to that level
void CheckLevelChange()
{
	if(keys['1'])
	{
		g_levelNumber = 3;
		g_LevelCompleted=false;
		resetGame();
	}
	else if(keys['2'])
	{
		g_levelNumber = 2;
		g_LevelCompleted=false;
		resetGame();
	}
	else if(keys['3'])
	{
		g_levelNumber = 3;
		g_LevelCompleted=false;
		resetGame();
	}
	else if(keys['4'])
	{
		g_levelNumber = 4;
		g_LevelCompleted=false;
		resetGame();
	}
	else if(keys['5'])
	{
		g_levelNumber = 5;
		g_LevelCompleted=false;
		resetGame();
	}
	else if(keys['6'])
	{
		g_levelNumber = 6;
		g_LevelCompleted=false;
		resetGame();

	}
	else if(keys['7'])
	{
		g_levelNumber = 7;
		g_LevelCompleted=false;
		resetGame();
	}
	else if(keys['8'])
	{
		g_levelNumber = 8;
		g_LevelCompleted=false;
		resetGame();

	}
	else if(keys['9'])
	{
		g_levelNumber = 9;
		g_LevelCompleted=false;
		resetGame();

	}

}
void updateScene(void){
	//IF GAME IS DONE, PRESS ENTER TO RESTART
	if(keys[VK_RETURN]&&(g_LevelCompleted||g_LevelFailed))
			resetGame();

	CheckLevelChange();
	if(g_LevelCompleted||g_LevelFailed)
		return;
		
	//Update all the stuff here
		if(keys[VK_LEFT])
		{g_Actor->SpecialKeyboard(VK_LEFT,0,0);}
		if(keys[VK_RIGHT])
		{g_Actor->SpecialKeyboard(VK_RIGHT,0,0);}
		if(keys[VK_UP])
		{	g_Actor->SpecialKeyboard(VK_UP,0,0);}
		/*if(keys[VK_DOWN])
		{	g_Actor->SpecialKeyboard(VK_DOWN,0,0);}*/
		if(keys[VK_SPACE])
		{	g_Actor->SpecialKeyboard(VK_SPACE,0,0);}
	//USER FAILED LEVEL IF ACTOR STRENGTH IS ZERO
	if(g_Actor->mStrength==0)
	{g_LevelFailed = true; return;}
	int targetCount=0;
	for(unsigned int i=0;i<g_GameObjectsList.size();i++)
	{
		if(g_GameObjectsList[i]->mBodyType==Obj_Target)
			targetCount++;
		if(g_GameObjectsList[i]->mBodyType==Obj_PowerUp)
		{
			PowerUp *p = (PowerUp*)g_GameObjectsList[i];
			if(p->ShouldErase())
			{
				delete p;
				g_GameObjectsList.erase(g_GameObjectsList.begin()+i);
				i--;
				continue;
			}
		}
	}
	//IF ALL TARGETS ARE DESTROYED, LEVEL IS COMPLETE
		if(targetCount==0)
		{
			g_LevelCompleted = true;
			return;
		}
	//CHECK IF OBJECTS HIT BOUNDARY, REFLECT ACTORS AND TARGETS
	//DESTROY BULLETS
	for(unsigned int i=0;i<g_GameObjectsList.size();i++)
	{
		if(g_GameObjectsList[i]->mBodyType==Obj_Target)
			targetCount++;
		//Check if any object has gone out of bounds,then delete it
		if(g_GameObjectsList[i]->isOutOfBound() )
		{
			if((g_GameObjectsList[i]->mBodyType!=Obj_Actor)&&(g_GameObjectsList[i]->mBodyType!=Obj_Target))
			{delete g_GameObjectsList[i];
			g_GameObjectsList.erase(g_GameObjectsList.begin()+i);
			i--;continue;}
			else
			{
				if((g_GameObjectsList[i]->mBodyType==Obj_Actor)||(g_GameObjectsList[i]->mBodyType==Obj_Target))
				{
				
				float ix = g_GameObjectsList[i]->mPosition.x;
				
				float iy= g_GameObjectsList[i]->mPosition.y;
				ix = (((ix-0)/(SCRWIDTH-0))*2)-1;
				iy = (((iy-0)/(SCRHEIGHT-0))*2)-1;
				float h=-0.27;
				float k=-0.01;

				float nx = ix-h; 
				float ny = iy-k;
				float n = sqrt(nx*nx +ny*ny); 
				nx/=n;ny/=n; //Hack: Normal Vector, looks ok
				
				float vx = cos(TO_RADIAN(g_GameObjectsList[i]->mAngle+90)); 
				float vy = sin(TO_RADIAN(g_GameObjectsList[i]->mAngle+90)); //forward vector
				
				float dot = (vx*nx) + (vy*ny);
				vx  = vx -(2*dot*nx); //reflectance formula
				vy = vy -(2*dot*ny);

				n = sqrt(vx*vx+vy*vy);
				vx/=n;vy/=n;
				float angle = TO_DEGREE(acos(vy)); //reflected angle
				if(vx > 0)
					angle = 360- angle;
				g_GameObjectsList[i]->mAngle = angle;
				
				}
				
			}
		}
	

		g_GameObjectsList[i]->Update();
		
	}
	
}
int renderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for(unsigned int i=0;i<g_GameObjectsList.size();i++)
	{
	g_GameObjectsList[i]->Render();
		
	}
	//Drawing circle for test
	/*glPushMatrix();
	glLoadIdentity();
	gluLookAt(0.0,0.0,1.0, 
		      0.0,0.0,-1.0,
			  0.0f,1.0f,0.0f);
	glColor3f(0,1,0);
	float radius = 0.75f;
	glTranslatef(-0.27,-0.01,0);
	glBegin(GL_LINE_LOOP);
	for(int i = 0; i<360;i+=10)
	{
		glVertex3f(cos(TO_RADIAN(i))*radius,sin(TO_RADIAN(i))*radius,0);
		
	}
	glEnd();
	glPopMatrix();*/
	//end of drawing circle for test
	drawUI();

	
	return TRUE;
}

/***********WinMain********************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	BOOL	done=FALSE;								// Bool Variable To Exit Loop
	

	// Create Our OpenGL Window
	if (!CreateGLWindow("Viroidz",800,800,32,fullscreen))
	{
		return 0;									// Quit If Window Was Not Created
	}
	initGameObjects();
	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=TRUE;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			// Draw The Scene.  Watch For ESC Key And Quit Messages From DrawGLScene()
			//TBD: Calculate delta time and pass, use in all updates
			updateScene();
			if ((active && !renderScene()) || keys[VK_ESCAPE])	// Active?  Was There A Quit Received?
			{
				done=TRUE;							// ESC or DrawGLScene Signalled A Quit
			}
			else									// Not Time To Quit, Update Screen
			{
				SwapBuffers(hDC);					// Swap Buffers (Double Buffering)
			}
			
			
		}
	}

	// Shutdown
	KillGLWindow();									// Kill The Window
	return (msg.wParam);							// Exit The Program
}

