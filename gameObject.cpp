#include "gameObject.h"
#include "basics.h"

Body::~Body(){mBodyType = Obj_Invalid;}
//Test If Object hit the boundary used for reflection/destruction
bool Body::isOutOfBound()
{
	float h=-0.27;
	float k=-0.01;
	float radius = 0.7f;
	float x = (((mPosition.x-0)/(SCRWIDTH-0))*2)-1;
	float y = (((mPosition.y-0)/(SCRHEIGHT-0))*2)-1;		
	if(((x-h)*(x-h)+(y-k)*(y-k)) >= (radius*radius))
	{
		printf("\nObject is out of bounding circle");
		return true;
	}
	return false;

}
void Body::Render(){}
void Body::Update(){}
void Body::SpecialKeyboard(int theKey, int mouseX, int mouseY){}